# Walt Disney Cards

Colección de películas de Walt Disney y Pixar.

---

Partiendo de la base que tengo en programación 💻 ( Html, CSS, JavaScript ) he creado un pequeño juego de memoria, consiste en recordar donde estan los pares de cartas iguales y emparejarlas, todo ello con un contador de tiempo y de movimientos realizados.

✅ Para ello he creado un index.html lo más correcto posible, con una estructura sencilla.

✅ El siguiente paso fue ponerse con el archivo .js que será el encargado de realizar todo lo necesario para lo que queremos realizar en nuestro juego .

✅ Y para finalizar, he realizado el diseño, gracias al documento style.css le doy una imagen visual a la creación, tratando de proporcionarle un aspecto interesante, que no dañe demasiado la vista 👀 .

---

### **Autor ✒️ :**

Todo el trabajo ha sido creado por :

- <span style="color:green">Marcos</span> : [LinkedIn](https://es.linkedin.com/in/marcos-v%C3%A1zquez-gonz%C3%A1lez-44379562?trk=people-guest_people_search-card)
  ➡️ Creador de la estructura base en `html` y de diseñar la parte visual de `CSS`.

---

## Funcionalidades :

> - 100% Responsive. </br>
> - Titulos originales. </br>
> - Poster / caratula de la película. </br>

---

## Web de la Aplicación ⚙ :

- [Link](https://disneymovies-three.vercel.app/)

---


